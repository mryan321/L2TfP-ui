# UI for Learn to Type for Programmers

For a general intro into L2T4P see https://gitlab.com/mryan321/L2TfP-infra

I am no frontend developer but will use this project to play around with some styling and web frameworks like React.

## Plan

- React / Redux (because of https://medium.com/@tkssharma/facebooks-flux-vs-redux-d02b5348cc17)
- Webpack - package
- Gulp - build
- npm - dependency management

## Architecture

- Kubernetes with nginx in front serving static assets and terminating SSL
- Maybe look at Google CDN (direct from bucket?) for static assets
- Then understand services like Akamai

## References

- https://medium.com/@tiangolo/react-in-docker-with-nginx-built-with-multi-stage-docker-builds-including-testing-8cc49d6ec305 and https://hub.docker.com/r/tiangolo/node-frontend/

Will start right off at level zero with https://github.com/facebook/create-react-app